describe("Test todo ", () => {

    before(()=>{
        cy.visit('/');
    })

    it("Enter data", () => {
        cy.get("input").type('this is test{enter}')
    })

    it('Check css', () => {
        cy.get('li > button img').should('not.be.visible')

        cy.get('ul > li').then(($element) => {

            const win = $element[0].ownerDocument.defaultView
            const before = win.getComputedStyle($element[0], 'before')
            cy.get(before).should('not.be.visible')

        })
        let width;

        cy.get('ul > li').invoke('width').then(function (owidth) {
            width = owidth
        });

        cy.get('ul > li').trigger('mouseover').then($els => {
            const win = $els[0].ownerDocument.defaultView
            const before = win.getComputedStyle($els[0], 'before')
            const contentValue = before.getPropertyValue('width')
            expect(contentValue).to.eq(width + 'px')
        })
    });

    it('Remove data', ()=> {
        cy.get('ul > li button').click()
    });

})