import React, {useReducer, useRef} from "react";

export default function Home() {
    const inputRef = useRef();

    const [items, dispatch] = useReducer((state, action) => {
        switch (action.type) {
            case "add":
                return [
                    ...state,
                    {
                        id: state.length,
                        name: action.name
                    }
                ];
            case "remove":
                // keep every item except the one we want to remove
                return state.filter((_, index) => index != action.index);
            default:
                return state;
        }
    }, []);

    function handleSubmit(e) {
        e.preventDefault();
        dispatch({
            type: "add",
            name: inputRef.current.value
        });
        inputRef.current.value = "";
    }

    return (
        <div className={"container"}>
            <form onSubmit={handleSubmit}>
                <input ref={inputRef} placeholder={"Add ToDo ..."}/>
            </form>
            <ul>
                {items.map((item, index) => (
                    <li key={item.id}>
                        {item.name}{" "}
                        <button onClick={() => dispatch({type: "remove", index})}>
                            <img src="/trash.png" alt="*"/>
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
}
